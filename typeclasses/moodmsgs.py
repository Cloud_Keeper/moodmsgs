"""
Mood Message Emitter.

Cloud_Keeper 2017

This is a series of mixins that set up rooms to emit mood messages set on
itself and it's contents at a set interval.

Commands:
-@moodmsg

Mixins:
-MoodObject
-MoodRoom

Use:
In typeclasses/objects:
    from typeclasses.moodmsgs import MoodObject

    class Object(MoodObject, DefaultObject):

In typeclasses/characters:
    from typeclasses.moodmsgs import MoodObject

    class Character(MoodObject, DefaultCharacter):

In typeclasses/rooms:
    from typeclasses.moodmsgs import MoodObject, MoodRoom

    class Room(MoodObject, MoodRoom, DefaultRoom):

In commands/default_cmdsets:
    from typeclasses.moodmsgs import MoodMsgCmdSet

    class CharacterCmdSet(default_cmds.CharacterCmdSet):

        def at_cmdset_creation(self):
            ...
            self.add(MoodMsgCmdSet())

TODO:
-Command to set your own values, if bulider of others values and interval
-command to start and stop messages
-rejig the way it stops repeats. if list is [x,y] should get x-y-x-y...etc
"""

import random
from evennia import DefaultRoom, TICKER_HANDLER, DefaultObject
from django.conf import settings
from evennia import CmdSet
from evennia.players.models import PlayerDB
from evennia.utils import create, search, utils, evtable

COMMAND_DEFAULT_CLASS = utils.class_from_module(settings.COMMAND_DEFAULT_CLASS)

##################################
# Flavour Commands
##################################


class MoodMsgCmdSet(CmdSet):
    """
    Holds commands used by Mood Messages.
    Import this to the character cmdset to gain access to Flavourtext commands.
    """

    def at_cmdset_creation(self):
        self.add(CmdMoodMsg())


def get_mood_list(obj):
    # Retrieve mood messages from object.
    msgs = obj.attributes.get("mood_msgs", default=[])
    if not msgs:
        return "This object does not have Mood Messages."

    # Return display table.
    table = evtable.EvTable(border="header")
    for msg in msgs:
        table.add_row(msg)
    return "|wMood Messages attached to this object:\n%s" % table


class CmdMoodMsg(COMMAND_DEFAULT_CLASS):
    """
    Interact with the Mood msg system.

    Usage:
        @moodmsg[/switch] <object> [= <message>]
    """

    key = "@moodmsg"
    locks = "cmd:all()"
    help_category = "Comms"

    def func(self):
        caller = self.caller
        switches = self.switches
        args = self.args

        if switches:
            # Ensure we have working msg and object with mood_msg attribute.
            if not self.rhs:
                caller.msg("Usage: @moodmsg[/switch] <object> [= <message>]")
                return

            msg = self.rhs
            if self.lhs == "self":
                obj = caller
            else:
                obj = caller.search(self.lhs, location=caller.location)
            if not obj:
                return
            if not (obj.access(caller, "control") or obj.access(caller, 'edit')):
                caller.msg("You don't have permission to do that.")
                return
            if not obj.db.mood_msgs:
                obj.db.mood_msgs = []

            # Add to objects Mood Messages.
            if "add" in switches:
                obj.db.mood_msgs.append(msg)
                caller.msg("Message added: " + msg)
                return

            # Remove from objects Mood Messages.
            if "remove" in switches:
                if msg not in obj.db.mood_msgs:
                    caller.msg("Remove aborted - No message matching: " + msg)
                    return
                obj.db.mood_msgs.remove(msg)
                caller.msg("Message removed: " + msg)
                return

        # Return Moodmsg list.
        if not args or args == "self":
            caller.msg(get_mood_list(caller))
            return

        obj = caller.search(args, location=caller.location)
        if not obj:
            return

        if not (obj.access(caller, "control") or obj.access(caller, 'edit')):
            caller.msg("You don't have permission to do that.")
            return
        caller.msg(get_mood_list(obj))


class MoodObject(object):

    def at_object_creation(self):
        self.db.mood_msgs = []
        super(MoodObject, self).at_object_creation()


class MoodRoom(object):
    """This room is ticked at regular intervals"""

    def at_object_creation(self):
        """called only when the object is first created"""
        self.db.mood_history = [None]*5
        self.db.mood_interval = 30
        TICKER_HANDLER.add(self.db.mood_interval, self.at_mood_update)
        super(MoodRoom, self).at_object_creation()

    def at_mood_update(self, *args, **kwargs):
        """ticked at regular intervals"""

        # Collate available flavour strings.
        msglist = []
        msglist.extend(self.db.mood_msgs)
        for obj in self.contents:
            msglist.extend(obj.attributes.get("mood_msgs", default=[]))

        # Rule out repeats unless not possible.
        finallist = [text for text in msglist if text not in self.db.mood_history]
        if not finallist:
            finallist = msglist
        if not finallist:
            return

        # Emit random flavour string. Update history.
        emit = random.choice(finallist)
        self.db.mood_history.insert(0, emit)
        self.db.mood_history.pop()
        self.msg_contents(emit)

    def change_mood_interval(self, interval):
        TICKER_HANDLER.remove(self.db.mood_interval, self.at_mood_update)
        self.db.mood_interval = interval
        TICKER_HANDLER.add(self.db.mood_interval, self.at_mood_update)
