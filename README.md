Mood Message Emitter.

This is a series of mixins that set up rooms to emit mood messages set on
itself and it's contents at a set interval.

Files:
typeclasses/moodmsgs.py

Notes:
For Possible change in approach see below converation:
8:32 PM] Cloud_Keeper: Looks like every object has a list of messages
[8:32 PM] Cloud_Keeper: Then rooms at a certain frequency collects all the messages of their objects and picks one at random
[8:35 PM] Cloud_Keeper: I remember I was trying to make it such that rooms had a list and objects would update the list, taking strings from it or adding to it as they left or entered the room
[8:35 PM] Cloud_Keeper: Then I thought, why do I need to keep an updated list at all, I'll just collect it each time.
[8:36 PM] Grin: Mine grabs a list of all the online players' puppets and from that builds another list of rooms that need to receive lines. 

For each room on that list it determines the current daytime, weather and season states, then using those it fetches the room's unique lines, the connected area's custom environment lines, and the default weather lines. Finally it picks one at random to display.
[8:37 PM] Grin: I've got a sort of cascading thing going for my weather simulation. Worlds have Continents, which have Regions, which have Areas, which contain Rooms. Means I can build other planets and stuff on the other side of the planet later on if I want (which I totally will)
[8:37 PM] Cloud_Keeper: That's genius
[8:38 PM] Cloud_Keeper: I thought about rooms checking if they had players in them, but I never thought about going through players to work out which rooms